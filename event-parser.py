from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
from flask_jsonpify import jsonify

from lib.queue.queue import Queue
from lib.event.event import Event

class EventParser():
    def __init__(self):
        self.queue = Queue()
        self.buffer  = []
        self.app  = Flask('test name')
        self.api = Api(self.app)
        self.api.add_resource(Event, '/')


if __name__ == '__main__':
    ep = EventParser()
    ep.app.run(port='5000')
    # q = ep.queue
    # q.add('1')
    # q.add('2')
    # q.add('3')
    # q.prepend('4')
    #
    # print(q.pop())
    # print(q.pop())
    # print(q.pop())
    # print(q.pop())
