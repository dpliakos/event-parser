# TODO: Define a Value object
class Value(object):
    def __init__(self, value):
        self.value = value

class SensorValue(object):
    def __init__(self, value, sensor):
        super().__init__(value)
        self.sensor = sensor

class AppValue(object):
    def __init__(self, value, app):
        super().__init__()
        self.app = app
