'''
Represents a physical sensor
'''

class Sensor(object):
    def __init__(self, id, group, device):
        self.id = id
        self.group = group
        self.device = device
