"""
Request is a message comming from the ouside world.
"""

"""
Request class
# TODO: define me
params:
  source - source interface
  target - target interface
"""
class Request(object):
    def __init__(self, source, target):
        self.source = source
        self.target = target

"""
Value request class
Represents a request containing a value
# TODO: define me, better

params:
    as above
    value the value that it represents
"""
class ValueRequest(Request):
    def __init__(self, source, target, value):
        super().__init__()
        self.value = value
