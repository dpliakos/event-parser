'''
The application communicates using message objects.
'''

class Message():
    def __init__(self, message):
        self.message = message

class OK(Message):
    def __init__(self, message):
        super()__init__(message)
        self.code = 200

class ERROR(Message):
    def __init__(self, message, code):
        super()__init__(message)
        self.code = code

# TODO: define error code
